﻿# API Data Transfer Objects

This project contains data transfer objects (complex model classes) corresponding to the Beckman Comms API output.

The DTO project should be repackaged and published with each update to the `Api.DataTransferObjects` assembly.

## NuGet Packaging Instructions

Visual Studio 2017 supports creation of NuGet packages from the IDE.
See <https://docs.microsoft.com/en-us/nuget/quickstart/create-and-publish-a-package-using-visual-studio> for details.

Publish package to `G:\NuGetFeed\BeckmanComms.Api.DataTransferObjects.{version}`.

### Resources

* [How to host your own NuGet Server and Package Feed](https://www.hanselman.com/blog/HowToHostYourOwnNuGetServerAndPackageFeed.aspx)
* [Publish your .Net library as a NuGet package](http://twistedoakstudios.com/blog/Post1295_publish-your-net-library-as-a-nuget-package)
* [Creating a local NuGet repository with dependencies bundles](http://www.marcusoft.net/2011/09/creating-local-nuget-repository-with.html)
 
## Using the NuGet Package

0. In Visual Studio, open your NuGet package sources config (**Tools > Options > NuGet Package Manager > Package Sources**).
1. Click the **+** icon to add a new source with default settings.
    * In the **Name** field, give the feed a meaningful name (e.g., "Beckman Internal Feed").
    * Use the **...** icon next to the **Source** field to browse to `G:\NuGetFeed`.
2. Click **Update**.
3. Click **OK**.
4. Search and install NuGet packages as usual.