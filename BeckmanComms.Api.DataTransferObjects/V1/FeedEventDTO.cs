﻿using System;
using System.Xml.Serialization;

namespace BeckmanComms.Api.DataTransferObjects.V1
{
    /// <summary>
    /// Data Transfer Object representing a Beckman event.
    /// </summary>
    [XmlType(TypeName = "Event")]
    public class FeedEventDTO
    {
        /// <summary>
        /// Event name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Room number in the Beckman building.
        /// </summary>
        public string RoomNumber { get; set; }

        /// <summary>
        /// Date and time of event.
        /// </summary>
        public DateTime DateTime { get; set; }

        /// <summary>
        /// Event date.
        /// </summary>
        public DateTime Date { get; set; }
    }
}