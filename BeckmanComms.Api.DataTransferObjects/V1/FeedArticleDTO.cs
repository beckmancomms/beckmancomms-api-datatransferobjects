﻿using System;
using System.Xml.Serialization;

namespace BeckmanComms.Api.DataTransferObjects.V1
{
    /// <summary>
    /// Data Transfer Object representing a Beckman news article.
    /// </summary>
    [XmlType(TypeName = "Article")]
    public class FeedArticleDTO
    {
        /// <summary>
        /// Article title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Article author's name.
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// Article URL.
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// Article description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Date the article was published.
        /// </summary>
        public DateTime PublishDate { get; set; }

        /// <summary>
        /// Flag indicating whether the article is hosted on a Beckman server.
        /// </summary>
        public bool IsInternal { get; set; }
    }
}