﻿using System.Xml.Serialization;

namespace BeckmanComms.Api.DataTransferObjects.V1
{
    /// <summary>
    /// Data Transfer Object representing a Beckman Institute staff member.
    /// </summary>
    [XmlType(TypeName = "Person")]
    public class FeedPersonDTO
    {
        /// <summary>
        /// Person's unique ID number.
        /// </summary>
        public long PersonId { get; set; }

        /// <summary>
        /// Person's full name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Person's Beckman title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Person's campus address.
        /// </summary>
        public string BuildingAddress { get; set; }

        /// <summary>
        /// Person's campus email address.
        /// </summary>
        public string EmailAddress { get; set; }

        /// <summary>
        /// Person's room number in the Beckman Institute.
        /// </summary>
        public string RoomNumber { get; set; }

        /// <summary>
        /// Person's campus phone number.
        /// </summary>
        public string PhoneNumber { get; set; }
    }
}