﻿using System;
using System.Xml.Serialization;

namespace BeckmanComms.Api.DataTransferObjects.V1
{
    /// <summary>
    /// Data Transfer Object representing a Beckman Cafe menu.
    /// </summary>
    [XmlType(TypeName = "CafeMenu")]
    public class FeedCafeMenuDTO
    {
        /// <summary>
        /// Menu title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Menu date.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Menu items.
        /// </summary>
        public string Description { get; set; }
    }
}