﻿namespace BeckmanComms.Api.DataTransferObjects.V2.Directory
{
    /// <summary>
    /// Data transfer object representing a staff member's publication record.
    /// </summary>
    public class PublicationDto
    {
        /// <summary>
        /// Bibliographic entry for publication.
        /// </summary>
        public string Bibliography { get; set; }

        /// <summary>
        /// Publication year.
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Link to publication.
        /// </summary>
        public string Link { get; set; }
    }
}