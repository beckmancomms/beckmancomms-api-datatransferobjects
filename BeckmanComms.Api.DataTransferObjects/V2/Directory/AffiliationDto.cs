﻿namespace BeckmanComms.Api.DataTransferObjects.V2.Directory
{
    /// <summary>
    /// Data Transfer object representing Beckman staff group membership.
    /// </summary>
    public class AffiliationDto
    {
        /// <summary>
        /// Group name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Group abbreviation.
        /// </summary>
        public string Abbreviation { get; set; }
    }
}