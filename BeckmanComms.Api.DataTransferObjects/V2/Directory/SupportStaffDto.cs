﻿namespace BeckmanComms.Api.DataTransferObjects.V2.Directory
{
    /// <summary>
    /// Data Transfer Object representing a Beckman group support staff member.
    /// </summary>
    public class SupportStaffDto
    {
        /// <summary>
        /// Staff member's U of I NetID.
        /// </summary>
        public string NetId { get; set; }

        /// <summary>
        /// Staff member's name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Abbreviation of the group supported by the staff member.
        /// </summary>
        public string GroupAbbreviation { get; set; }
    }
}