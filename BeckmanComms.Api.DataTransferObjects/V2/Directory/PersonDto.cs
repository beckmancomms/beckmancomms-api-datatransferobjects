﻿using System.Collections.Generic;

namespace BeckmanComms.Api.DataTransferObjects.V2.Directory
{
    /// <summary>
    /// Data Transfer Object containing combined directory database fields.
    /// </summary>
    public class PersonDto
    {
        /// <summary>
        /// Staff member's U of I NetID.
        /// </summary>
        public string NetId { get; set; }

        /// <summary>
        /// Staff member's name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Staff member's preferred first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Staff member's family name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Staff member's email address.
        /// </summary>
        public string EmailAddress { get; set; }

        /// <summary>
        /// Staff member's primary group abbreviation.
        /// </summary>
        public string PrimaryGroupAbbreviation { get; set; }

        /// <summary>
        /// Staff member's title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Staff member's employment/affiliation status.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Staff member's home university unit.
        /// </summary>
        public string HomeUnit { get; set; }

        /// <summary>
        /// Staff member's campus address.
        /// </summary>
        public string BuildingAddress { get; set; }

        /// <summary>
        /// Staff member's office number.
        /// </summary>
        public string RoomNumber { get; set; }

        /// <summary>
        /// Staff member's campus phone number.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Staff member's HTML-formatted biography.
        /// </summary>
        public string Biography { get; set; }

        /// <summary>
        /// HTML-formatted statement of honors received by staff member.
        /// </summary>
        public string Honors { get; set; }

        /// <summary>
        /// HTML-formatted summary of staff member's research.
        /// </summary>
        public string Research { get; set; }

        /// <summary>
        /// Collection containing staff member's related links.
        /// </summary>
        public IEnumerable<LinkDto> Links { get; set; }

        /// <summary>
        /// Collection containing staff member's press mentions.
        /// </summary>
        public IEnumerable<PressDto> Press { get; set; }

        /// <summary>
        /// Collection containing staff member's publications.
        /// </summary>
        public IEnumerable<PublicationDto> Publications { get; set; }

        /// <summary>
        /// Collection containing staff member's directory photos.
        /// </summary>
        public IDictionary<string, PhotoDto> Photos { get; set; }

        /// <summary>
        /// Collection containing staff member's group affiliations.
        /// </summary>
        public IEnumerable<AffiliationDto> Affiliations { get; set; }

        public int Priority { get; set; }
    }
}