﻿using System.Collections.Generic;

namespace BeckmanComms.Api.DataTransferObjects.V2.Directory
{
    /// <summary>
    /// Data Transfer Object representing a Beckman group.
    /// </summary>
    public class GroupDto
    {
        /// <summary>
        /// Group name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Group abbreviation.
        /// </summary>
        public string Abbreviation { get; set; }

        /// <summary>
        /// Parent group abbreviation.
        /// </summary>
        public string ParentAbbreviation { get; set; }

        /// <summary>
        /// Internal group category designation (e.g., Administrative).
        /// </summary>
        public string GroupType { get; set; }

        /// <summary>
        /// Collection containing group members.
        /// </summary>
        public IEnumerable<PersonDto> Members { get; set; }

        /// <summary>
        /// Collection containing group leaders.
        /// </summary>
        public IEnumerable<GroupLeaderDto> Leaders { get; set; }

        /// <summary>
        /// Collection containing group support staff.
        /// </summary>
        public IEnumerable<SupportStaffDto> SupportStaff { get; set; }
    }
}