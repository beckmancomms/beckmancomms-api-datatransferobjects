﻿namespace BeckmanComms.Api.DataTransferObjects.V2.Directory
{
    /// <summary>
    /// Data Transfer Object representing a staff directory photo.
    /// </summary>
    public class PhotoDto
    {
        /// <summary>
        /// Photo's url.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Description of photo for use with required HTML alt attributes.
        /// </summary>
        public string AltText { get; set; }
    }
}