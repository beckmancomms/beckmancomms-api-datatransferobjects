﻿namespace BeckmanComms.Api.DataTransferObjects.V2.Directory
{
    /// <summary>
    /// Data transfer object representing a link.
    /// </summary>
    public class LinkDto
    {
        /// <summary>
        /// Human readable link title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Link URL.
        /// </summary>
        public string Url { get; set; }
    }
}