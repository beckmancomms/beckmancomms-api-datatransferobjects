﻿namespace BeckmanComms.Api.DataTransferObjects.V2.Directory
{
    /// <summary>
    /// Data transfer object representing a press mention.
    /// </summary>
    public class PressDto
    {
        /// <summary>
        /// Article title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Link to article.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Article publication year.
        /// </summary>
        public int Year { get; set; }
    }
}