﻿namespace BeckmanComms.Api.DataTransferObjects.V2.Directory
{
    /// <summary>
    /// Data Transfer Object representing a Beckman group leader.
    /// </summary>
    public class GroupLeaderDto
    {
        /// <summary>
        /// Leader name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Leader title.
        /// </summary>
        public string Title { get; set; }
    }
}