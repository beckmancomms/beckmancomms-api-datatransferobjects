﻿using System;

namespace BeckmanComms.Api.DataTransferObjects.V2.Cafe
{
    /// <summary>
    /// Data Transfer Object representing a cafe announcement.
    /// </summary>
    public class CafeAnnouncementDto
    {
        /// <summary>
        /// Date to post the announcement.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Message to be displayed in the announcement.
        /// </summary>
        public string Message { get; set; }
    }
}