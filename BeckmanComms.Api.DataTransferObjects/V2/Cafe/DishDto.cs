﻿using System.Collections.Generic;

namespace BeckmanComms.Api.DataTransferObjects.V2.Cafe
{
    /// <summary>
    /// Data Transfer Object representing a
    /// </summary>
    public class DishDto
    {
        /// <summary>
        /// The name of the dish.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// The name of the dish.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The category of the dish (e.g., Soup, Sandwich, Salad).
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// The description of the dish.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The path to the image of the dish.
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// The description of the image of the dish, suitable for use as alt text.
        /// </summary>
        public string ImageDescription { get; set; }

        /// <summary>
        /// The standard price of the dish.
        /// </summary>
        public decimal StandardPrice { get; set; }

        /// <summary>
        /// The price of a half-order of the dish, when available.
        /// </summary>
        public decimal? HalfPrice { get; set; }

        /// <summary>
        /// Indicates whether the item is available every day ( <c>DishDto.Specialty == false</c>) or
        /// as an occasional specialty item ( <c>DishDto.Specialty == true</c>).
        /// </summary>
        public bool Specialty { get; set; }

        /// <summary>
        /// Indicates whether the item is in-stock ( <c>DishDto.SoldOut == false</c>) or sold-out (
        /// <c>DishDto.SoldOut == true</c>).
        /// </summary>
        public bool SoldOut { get; set; }

        /// <summary>
        /// Indicates whether the item should not be displayed as a featured item (
        /// <c>DishDto.Featured == false</c>) or should be displayed as a featured item (
        /// <c>DishDto.SoldOut == true</c>).
        /// </summary>
        public bool Featured { get; set; }

        /// <summary>
        /// The qualities of a dish (e.g., Vegetarian, Vegan, Gluten Free).
        /// </summary>
        public IEnumerable<DishQualityDto> DishQualities { get; set; }
    }
}