﻿namespace BeckmanComms.Api.DataTransferObjects.V2.Cafe
{
    /// <summary>
    /// Data Transfer Object representing a dish quality.
    /// </summary>
    /// <example>Gluten Free</example>
    /// <example>Spicy</example>
    /// <example>Vegan</example>
    public class DishQualityDto
    {
        /// <summary>
        /// The name of the dish quality.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The icon representing the dish quality.
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// The description of the icon representing the dish quality.
        /// </summary>
        public string IconDescription { get; set; }
    }
}