﻿using System;
using System.Collections.Generic;

namespace BeckmanComms.Api.DataTransferObjects.V2.Cafe
{
    /// <summary>
    /// Data Transfer Object containing combined menu database fields.
    /// </summary>
    public class MenuDto
    {
        /// <summary>
        /// Menu date in a human-readable format.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Menu date.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Collection containing dishes that belong to this menu.
        /// </summary>
        public IEnumerable<DishDto> Dishes { get; set; }
    }
}