﻿using System.ComponentModel;

namespace BeckmanComms.Api.DataTransferObjects.V2.Events
{
    /// <summary>
    /// Names and calendar IDs of Beckman Institute event calendars.
    /// </summary>
    public enum BeckmanCalendars
    {
        /// <summary>
        /// Main Beckman Institute calendar.
        /// </summary>
        [Description("The main Beckman Institute event calendar.")]
        BeckmanInstitute = 4595,

        /// <summary>
        /// University holiday calendar
        /// </summary>
        [Description("The uofi holiday calendar.")]
        Holidays = 468,

        /// <summary>
        /// CNL calendar.
        /// </summary>
        [Description("Calendar for the Cognitive Neuroimaging Laboratory (CNL) at the Beckman Institute.")]
        CnlEvents = 4521,

        /// <summary>
        /// ILLI calendar.
        /// </summary>
        [Description("Calendar for the Illinois Language and Literacy Initiative (ILLI) at the Beckman Institute.")]
        IlliEvents = 5511,

        /// <summary>
        /// LBC calendar.
        /// </summary>
        [Description("Calendar for the Lifelong Brain and Cognition Laboratory (LBC) at the Beckman Institute.")]
        LbcLab = 4669,

        /// <summary>
        /// MSL calendar.
        /// </summary>
        [Description("Calendar for the Memory Systems Lab (MSL) at the Beckman Institute.")]
        MslEvents = 4470,

        /// <summary>
        /// Beckman digital signs calendar feed.
        /// </summary>
        [Description("Beckman Institute calendar for use on in-house digital signs. Contains non-general-interest events.")]
        BeckmanInstituteDigitalSigns = -1
    }
}