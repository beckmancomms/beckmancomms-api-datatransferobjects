﻿using System;

namespace BeckmanComms.Api.DataTransferObjects.V2.Events
{
    /// <summary>
    /// Data Transfer Object representing a Beckman Institute event.
    /// </summary>
    public class EventDto
    {
        /// <summary>
        /// Event's unique identifier.
        /// </summary>
        public int EventId { get; set; }

        /// <summary>
        /// ID for calendar containing event.
        /// </summary>
        public string CalendarId { get; set; }

        /// <summary>
        /// Event title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Event description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Starting date and time of event.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Ending date and time of event.
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Event location.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Name of event's primary contact.
        /// </summary>
        public string ContactName { get; set; }

        /// <summary>
        /// Event contact's email address.
        /// </summary>
        public string ContactEmail { get; set; }

        /// <summary>
        /// Event contact's phone number.
        /// </summary>
        public string ContactPhone { get; set; }

        /// <summary>
        /// Event sponsor.
        /// </summary>
        public string Sponsor { get; set; }

        /// <summary>
        /// Type of event.
        /// </summary>
        public string EventType { get; set; }

        /// <summary>
        /// Whether event is recurring.
        /// </summary>
        public bool Recurring { get; set; }

        /// <summary>
        /// Shared identifier linking recurring events.
        /// </summary>
        public int? RecurrenceId { get; set; }
    }
}