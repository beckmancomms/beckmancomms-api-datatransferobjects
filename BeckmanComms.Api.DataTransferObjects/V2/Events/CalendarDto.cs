﻿namespace BeckmanComms.Api.DataTransferObjects.V2.Events
{
    /// <summary>
    /// Information describing a webtools calendar.
    /// </summary>
    public class CalendarDto
    {
        /// <summary>
        /// Calendar Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Webtools calendar ID.
        /// </summary>
        public int CalendarId { get; set; }

        /// <summary>
        /// Human-readable description of calendar's purpose.
        /// </summary>
        public string Description { get; set; }
    }
}